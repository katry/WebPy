from .conftest import Server
from requests import get


def test_headers():
	def app_wrapper():
		from weepy import ASGI, Route
		app = ASGI(content_type="application/json")

		@Route("/header", content_type="text/plain")
		async def header(req, resp):
			resp.add_header("test", "12345")
			return ""

		@Route("/header-req", content_type="text/plain")
		async def header_req(req, resp):
			header = req.headers.get("jezevec")
			return "header=" + str(header)

		return app

	server = Server(app_wrapper)

	response = get("http://localhost:3456/header")
	assert response.status_code == 200
	assert response.headers["Content-Type"][0:10] == "text/plain"
	assert response.text == ""
	assert response.headers["test"] == "12345"

	response = get("http://localhost:3456/header-req", headers={"jezevec": "pes"})
	assert response.status_code == 200
	assert response.headers["Content-Type"][0:10] == "text/plain"
	assert response.text == "header=pes"

	server.stop()
